#!/usr/bin/env python3
#
# This is a program that generates keysmashes
# using markov chain.
#
import random, json, math, sys

# Generate a markov chain based on the keysmashing
def _generate_markov_chain(s):
    chain = {}
    for i, c in enumerate(s[:-1]):
        nc = s[i+1]
        if c in '\t\n' or nc in '\t\n': continue
        if c not in chain: chain[c] = {}

        if nc not in chain[c]: chain[c][nc] = 1
        else:                  chain[c][nc] += 1

    for c in chain:
        c = chain[c]
        total = sum(c[x] for x in c)
        for nc in c:       c[nc] /= total
        
    return chain

class KeysmashChain:
    def __init__(self, keysmash):
        if type(keysmash) not in [str, dict]:
            raise TypeError("keysmash must be a string or a dict.")

        if type(keysmash) == str:
            self.data  = keysmash
            self.chain = _generate_markov_chain(self.data)
        elif type(keysmash) == dict:
            self.data  = None
            self.chain = keysmash
        self.cchar = random.choice(list(self.chain))

    def next(self):
        r = random.random()
        if self.cchar not in self.chain:
            self.cchar = random.choice(list(self.chain))
        else:
            cc = self.chain[self.cchar]
            self.cchar = random.choices(
                list(cc), weights = [cc[x] for x in cc])[0]
        return self.cchar
    
    def generate(self, length: int):
        return ''.join(self.next() for x in range(length))

### CLI stuff ###

def _CLI_help(code = 1):
    print("""
pldf - :PLeaDing_Face:
pldf is a tool that generates keysmashing using the markov chain.

USAGE: pldf chain    IN OUT
         generates a JSON file with the markov chain.
         IN must be a plain text file containing keysmashing.
         OUT will be the JSON output.

       pldf generate FILENAME LENGTH
         Generates keysmashing from a JSON markov chain.
         FILENAME is the JSON file.
         LENGTH is the length of the keysmashing.
       pldf help
""".strip())
    sys.exit(code)

def _CLI_chain(inf = sys.stdin, ouf = sys.stdout):
    with open(inf) as IN:
        with open(ouf, "w") as OUT:
            ks = KeysmashChain(IN.read())
            OUT.write(json.dumps(ks.chain))

def _CLI_generate(fn, ln):
    if not ln.isdigit():
        print("input must be a digit", file=sys.stderr)
        sys.exit(1)

    with open(fn) as f:
        data = json.loads(f.read())
        ks = KeysmashChain(data)

        print(ks.generate(int(ln)))

def main():
    if len(sys.argv) <= 1: _CLI_help(1)
    elif sys.argv[1] == "chain" and len(sys.argv) == 4:
        _CLI_chain(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == "generate" and len(sys.argv) == 4:
        _CLI_generate(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == "help":
        _CLI_help(0)
    else:
        _CLI_help(1)

if __name__ == "__main__": main()
